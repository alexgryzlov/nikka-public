use std::{env, process::Command};

use anyhow::Result;


fn main() -> Result<()> {
    let out_dir = env::var("OUT_DIR")?;

    assert!(Command::new("as")
        .args(&["-o", &(out_dir.clone() + "/context.o"), "src/impl.s"])
        .status()?
        .success());

    assert!(Command::new("ar")
        .args(&[
            "-crus",
            &(out_dir.clone() + "/libcontext.a"),
            &(out_dir.clone() + "/context.o")
        ])
        .status()?
        .success());

    println!("cargo:rustc-link-search=native={}", out_dir);
    println!("cargo:rustc-link-lib=static=context");

    Ok(())
}
