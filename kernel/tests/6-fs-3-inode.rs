#![feature(asm_const)]
#![feature(custom_test_frameworks)]
#![feature(naked_functions)]
#![feature(slice_group_by)]
#![no_main]
#![no_std]
#![reexport_test_harness_main = "test_main"]
#![test_runner(kernel::test_runner)]


extern crate alloc;

use alloc::{format, vec, vec::Vec};
use core::{cmp, mem};

use chrono::Duration;

use ku::memory::size::{MiB, Size};

use kernel::{
    fs::{
        test_scaffolding::{flush_block, make_file, BLOCK_SIZE},
        File,
        Kind,
    },
    log::debug,
    memory::Virt,
    time::{self, TscDuration},
    Subsystems,
};


mod fs_helpers;
mod gen_main;


gen_main!(Subsystems::MEMORY);


#[test_case]
fn block_entry() {
    let (mut block_bitmap, mut inode) = fs_helpers::single_inode_fs(Kind::File);

    const ENTRY_SIZE: usize = mem::size_of::<usize>();
    const ENTRIES_PER_BLOCK: usize = BLOCK_SIZE / ENTRY_SIZE;

    let mut consecutive_entries = ENTRIES_PER_BLOCK - 1;
    let mut prev_entry = Virt::default();
    let check_entries = 3 * ENTRIES_PER_BLOCK.pow(2);

    for i in 0..check_entries {
        let entry = inode.block_entry(i, &mut block_bitmap).unwrap();

        let current_entry = Virt::from_ref(entry);
        let prev_entry_inc = (prev_entry + ENTRY_SIZE).unwrap();
        if prev_entry >= current_entry || i % 100_000 == 0 {
            debug!(i, %prev_entry, %current_entry);
        }
        assert!(prev_entry < current_entry);

        if consecutive_entries != 0 && prev_entry != Virt::default() {
            assert_eq!(current_entry, prev_entry_inc);
        }

        consecutive_entries = (consecutive_entries + 1) % ENTRIES_PER_BLOCK;
        prev_entry = current_entry;

        *entry = i;
    }

    debug!("block entry allocation is done, checking the entries");

    for i in 0..check_entries {
        let entry = inode.block_entry(i, &mut block_bitmap).unwrap();
        if *entry != i || i % 100_000 == 0 {
            debug!(i, actual = *entry, expected = i);
        }
        assert_eq!(*entry, i);
    }
}


#[test_case]
fn read_write_speed() {
    let (block_bitmap, inode) = fs_helpers::single_inode_fs(Kind::File);
    let mut file = make_file(&inode, &block_bitmap);

    let mut buffer = [0x77; MiB];
    let timer = time::timer();
    file.write(0, &buffer).unwrap();
    let elapsed = timer.elapsed();
    debug!(size = %Size::from_slice(&buffer), %elapsed, "disk read speed");

    buffer.fill(0);

    let timeout = TscDuration::try_from(Duration::milliseconds(200)).unwrap();

    let timer = time::timer();
    file.read(0, &mut buffer).unwrap();
    let elapsed = timer.elapsed();
    debug!(size = %Size::from_slice(&buffer), %elapsed, %timeout, "file system read speed");
    assert!(elapsed < timeout);

    assert_eq!(buffer[123456], 0x77);
    buffer.fill(0x33);

    let timer = time::timer();
    file.write(0, &buffer).unwrap();
    let elapsed = timer.elapsed();
    debug!(size = %Size::from_slice(&buffer), %elapsed, %timeout, "file system write speed");
    assert!(elapsed < timeout);

    let timer = time::timer();
    for block in 0..fs_helpers::BLOCK_COUNT {
        flush_block(block).unwrap();
    }
    let elapsed = timer.elapsed();
    debug!(size = %Size::from_slice(&buffer), %elapsed, "disk write speed");
}


#[test_case]
fn write_read() {
    let (block_bitmap, inode) = fs_helpers::single_inode_fs(Kind::File);
    let mut file = make_file(&inode, &block_bitmap);

    let data: Vec<_> = (0..MiB).map(|x| (x + x * x) as u8).collect();
    let mut offset = 0;

    while offset < data.len() {
        for size in (0..=3 * BLOCK_SIZE).filter(|x| (x + 3) % BLOCK_SIZE < 6) {
            let real_size = cmp::min(offset + size, data.len()) - offset;
            assert_eq!(
                file.write(offset, &data[offset..offset + real_size]),
                Ok(real_size),
            );
            offset += real_size;
        }
    }

    let mut buffer = [0; MiB];
    offset = 0;

    while offset < data.len() {
        for size in (0..=4 * BLOCK_SIZE).filter(|x| (x + 4) % BLOCK_SIZE < 7) {
            let real_size = cmp::min(offset + size, data.len()) - offset;
            assert_eq!(
                file.read(offset, &mut buffer[offset..offset + real_size]),
                Ok(real_size),
            );
            offset += real_size;
        }
    }

    debug!(actual = ?buffer[..10], expected = ?data[..10]);
    for (actual, expected) in buffer.iter().zip(data) {
        assert_eq!(*actual, expected);
    }
}


#[test_case]
fn big_file() {
    let (block_bitmap, inode) = fs_helpers::single_inode_fs(Kind::File);
    let mut file = make_file(&inode, &block_bitmap);
    let start_free_block_count = block_bitmap.free_block_count();

    let file_block_count = start_free_block_count * 99 / 100;
    file.set_size(file_block_count * BLOCK_SIZE).unwrap();
    debug!(file_block_count, file_size = %Size::bytes(file.size()));

    for file_block in 0..file_block_count {
        let data = format!("file block {file_block}");
        let data_len = data.as_bytes().len();
        assert_eq!(
            file.write(file_block * BLOCK_SIZE, data.as_bytes()),
            Ok(data_len),
        );
    }

    for file_block in 0..file_block_count {
        let data = format!("file block {file_block}");
        let data_len = data.as_bytes().len();
        let mut buffer = vec![0; data_len];
        assert_eq!(
            file.read(file_block * BLOCK_SIZE, &mut buffer),
            Ok(data_len),
        );
        assert_eq!(buffer, data.as_bytes());
    }

    let free_block_count = block_bitmap.free_block_count();
    let used_block_count = start_free_block_count - free_block_count;
    debug!(free_block_count, used_block_count);

    file.remove().unwrap();

    let end_free_block_count = block_bitmap.free_block_count();
    let leaked_block_count = start_free_block_count - end_free_block_count;
    assert_eq!(leaked_block_count, 0);
}


#[test_case]
fn set_size() {
    let (block_bitmap, inode) = fs_helpers::single_inode_fs(Kind::File);
    let mut file = make_file(&inode, &block_bitmap);
    let start_free_block_count = block_bitmap.free_block_count();

    let filter = |x: &usize| (x + 2) % (BLOCK_SIZE / 3) <= 4;

    for offset in (0..=2 * BLOCK_SIZE).filter(filter) {
        for size in (0..=2 * BLOCK_SIZE).filter(filter) {
            if offset == size {
                debug!(offset, size);
            }
            check_expansion_is_zero_filled(&mut file, offset, size);
            file.set_size(offset).unwrap();
            check_expansion_is_zero_filled(&mut file, offset, size);
            file.set_size(offset).unwrap();
        }
    }

    file.set_size(0).unwrap();
    let end_free_block_count = block_bitmap.free_block_count();
    let leaked_block_count = start_free_block_count - end_free_block_count;
    assert_eq!(leaked_block_count, 0);


    fn check_expansion_is_zero_filled(file: &mut File, offset: usize, size: usize) {
        let mut buffer = vec![1; size];

        file.set_size(offset + size).unwrap();
        file.read(offset, &mut buffer).unwrap();
        assert!(buffer.iter().all(|&x| x == 0));

        buffer.fill(b'*');
        assert_eq!(file.write(offset, &buffer), Ok(size));
        file.read(offset, &mut buffer).unwrap();
        assert!(buffer.iter().all(|&x| x == b'*'));
    }
}
